import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Hero } from '../hero';
import { HEROES } from '../mock-heroes';

import { MessageService } from '../message.service';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HeroesComponent implements OnInit {

  constructor(private heroService: HeroService) {}

  heroes: Hero[];
  selectedHero: Hero;


  ngOnInit() {
    console.log("HeroesComponent ngOnInit")
    this.getHeroes();
  }



  getHeroes(): void {
    this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes,err=> console.log(JSON.stringify(err)));

  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  showMessage(){
    alert("I am from heroes");
  }
}
