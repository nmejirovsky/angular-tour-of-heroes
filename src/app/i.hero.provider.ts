
import { Hero } from './hero';
//import { HEROES } from './mock-heroes';
import { Observable } from 'rxjs/Observable';
//import { of } from 'rxjs/observable/of';


export interface IHeroProvider {
  getHeroes(): Observable<Hero[]>;
  getHero(id: number): Observable<Hero>;

}
