import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Hero }         from '../hero';

import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

import { HeroDetailMdcontentComponent } from '../hero-detail-mdcontent/hero-detail-mdcontent.component';


//https://ng-bootstrap.github.io/#/components/modal/examples

@Component({
  selector: 'app-hero-detail-modal',
  templateUrl: './hero-detail-modal.component.html',
  styleUrls: ['./hero-detail-modal.component.css']
})
export class HeroDetailModalComponent  implements OnInit,AfterViewInit {
  @Input() hero: Hero;
  @Input() callback: Function;

  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {

  }

  ngAfterViewInit(): void  {
    const self = this;


  }

  open() {
    const modalRef = this.modalService.open(HeroDetailMdcontentComponent);
    modalRef.componentInstance.hero = this.hero;
  }
}
