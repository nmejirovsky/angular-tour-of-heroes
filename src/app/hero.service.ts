import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { HEROES } from './mock-heroes';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { MessageService } from './message.service';
import { IHeroProvider } from './i.hero.provider';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HeroMockService } from './hero.mock.service';
import { HeroHttpService } from './hero.http.service';



import { environment } from '../environments/environment';


@Injectable()
export class HeroService {
  private heroesProvider: IHeroProvider;

  constructor(private messageService: MessageService, private http: HttpClient) {
    if (environment.useMock)
      this.heroesProvider = new HeroMockService(messageService, http);
    else
      this.heroesProvider = new HeroHttpService(messageService, http);
  }

  getHeroes(): Observable<Hero[]> {
    return this.heroesProvider.getHeroes();

  }

  getHero(id: number): Observable<Hero> {
    return this.heroesProvider.getHero(id);

  }


}


