import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component';
//import { HeroService } from './hero.service';
import { HeroService } from './hero.service';

import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';
import { AppRoutingModule } from './/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeroDetailModalComponent } from './hero-detail-modal/hero-detail-modal.component';

import { HeroDetailMdcontentComponent } from './hero-detail-mdcontent/hero-detail-mdcontent.component';

import {HttpClientModule} from '@angular/common/http';
import { SetColorDirective } from './set-color.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    MessagesComponent,
    DashboardComponent,
    HeroDetailComponent,
    HeroDetailModalComponent,
    HeroDetailMdcontentComponent,
    SetColorDirective
  ],
  entryComponents: [HeroDetailModalComponent,HeroDetailMdcontentComponent],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    HttpClientModule
  ],
  providers: [ HeroService, MessageService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
