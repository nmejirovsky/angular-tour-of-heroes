import { Injectable } from '@angular/core';
import { IHeroProvider } from './i.hero.provider';
import { Hero } from './hero';
import { Observable } from 'rxjs/Observable';
import { MessageService } from './message.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable()
export class HeroHttpService  implements IHeroProvider {
  
    constructor(private messageService: MessageService, private http: HttpClient) { }

    getHeroes(): Observable<Hero[]> {
      console.log("Use Http");
      return new Observable(observer => {
          this.http.get(environment.apiUrl + "/heroes/", { }).subscribe(
          data => {
            this.messageService.add('HeroService: http fetched heroes');
            observer.next(<Hero[]>data);
            //observer.error("fail");
          },
          err => {
            this.messageService.add('HeroService: http fail fetched heroes ');
            observer.error("fail");
          }
        );
      });
      // Todo: send the message _after_ fetching the heroes
    }
    //http://localhost:5000/api?apiType=heroes_api&method=get_hero&params={%22id%22:11}
    getHero(id: number): Observable<Hero> {
      console.log("Use Http");
      return new Observable(observer => {
        
        this.http.get(environment.apiUrl + "/heroes/" + id, { }).subscribe(
          data => {
            this.messageService.add(`HeroService: http fetched hero id=${id}`);
  
            observer.next(<Hero>data);
            //observer.error("fail");
          },
          err => {
            this.messageService.add('HeroService: http fail fetched hero id=${id}');
            observer.error("fail");
          }
        );
      });
    }

}
