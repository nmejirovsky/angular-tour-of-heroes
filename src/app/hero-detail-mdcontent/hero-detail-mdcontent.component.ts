import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Hero }         from '../hero';

import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-hero-detail-mdcontent',
  templateUrl: './hero-detail-mdcontent.component.html',
  styleUrls: ['./hero-detail-mdcontent.component.css']
})
export class HeroDetailMdcontentComponent implements OnInit {
  @Input() hero;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {

  }

}
