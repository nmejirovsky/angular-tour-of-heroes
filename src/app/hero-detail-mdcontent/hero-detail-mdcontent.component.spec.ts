import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroDetailMdcontentComponent } from './hero-detail-mdcontent.component';

describe('HeroDetailMdcontentComponent', () => {
  let component: HeroDetailMdcontentComponent;
  let fixture: ComponentFixture<HeroDetailMdcontentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroDetailMdcontentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroDetailMdcontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
