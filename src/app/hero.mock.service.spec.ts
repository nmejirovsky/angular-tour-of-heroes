import { TestBed, inject } from '@angular/core/testing';

import { HeroMockService } from './hero.mock.service';

describe('Hero.MockService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeroMockService]
    });
  });

  it('should be created', inject([HeroMockService], (service: HeroMockService) => {
    expect(service).toBeTruthy();
  }));
});
