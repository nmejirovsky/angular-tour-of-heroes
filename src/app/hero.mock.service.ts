import { Injectable } from '@angular/core';
import { IHeroProvider } from './i.hero.provider';
import { Hero } from './hero';
import { Observable } from 'rxjs/Observable';
import { HEROES } from './mock-heroes';
import { MessageService } from './message.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient, HttpParams } from '@angular/common/http';


@Injectable()
export class HeroMockService implements IHeroProvider {

  constructor(private messageService: MessageService, private http: HttpClient) { }
  

  getHeroes(): Observable<Hero[]> {
    console.log("Use Mock");
    return new Observable(observer => {
      setTimeout(() => {
        this.messageService.add('HeroService: mock fetched heroes');
        observer.next(HEROES);
        //observer.error("fail");
      }, 0);

    });
    // Todo: send the message _after_ fetching the heroes

  }

  getHero(id: number): Observable<Hero> {
    console.log("Use Mock");
    return new Observable(observer => {
      setTimeout(() => {
        this.messageService.add(`HeroService: mocks fetched hero id=${id}`);
        let hero = HEROES.find(hero => hero.id === id);
        observer.next(hero);
        //observer.error("fail");
      }, 0);

    });

  }

}
