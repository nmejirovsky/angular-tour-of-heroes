import { Directive } from '@angular/core';
import { ElementRef } from '@angular/core';

@Directive({
  selector: '[appSetColor]'
})
export class SetColorDirective {

  constructor(private el: ElementRef) {
    el.nativeElement.style.backgroundColor = "red";
  }

}
